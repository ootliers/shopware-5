<?php

namespace Ootliers;

use Ootliers\Transport\OrderTransporter;
use Shopware\Components\Plugin;

class Ootliers extends Plugin
{
    public static function getSubscribedEvents()
    {
        return [
            'Shopware_Modules_Order_SaveOrder_OrderCreated' => 'onOrderCreate'
        ];
    }

    public function onOrderCreate(\Enlight_Event_EventArgs $args)
    {
        $orderDetails = [];
        $orderDetails['content'] = $args->get('details');
        $orderDetails['number'] = $args->get('orderNumber');
        $apiKey = $this->getConfigValue('apiKey');
        $siteId = $this->getConfigValue('siteId');

        $this->container->get(OrderTransporter::class)->transport($orderDetails, $siteId, $apiKey);
    }

    private function getConfigValue($configName)
    {
        $shop = false;
        if ($this->container->initialized('shop')) {
            $shop = $this->container->get('shop');
        }

        if (!$shop) {
            $shop = $this->container->get('models')->getRepository(\Shopware\Models\Shop\Shop::class)->getActiveDefault();
        }
        $config = $this->container->get('shopware.plugin.cached_config_reader')->getByPluginName('Ootliers', $shop);
        if (!isset($config[$configName])) {
            return null;
        }
        return $config[$configName];
    }
}
