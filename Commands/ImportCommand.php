<?php

namespace Ootliers\Commands;

use Ootliers\Transport\OrderTransporter;
use Shopware\Commands\ShopwareCommand;

use Shopware\Models\Order\Order;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ShopwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ootliers:export')
            ->setDescription('Import order data into Ootliers');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $apiKey = $this->getConfigValue('apiKey');
        $siteId = $this->getConfigValue('siteId');
        $em = $this->container->get('models')->getRepository(\Shopware\Models\Order\Order::class);
        $orders = $em->findAll();

        /** @var Order $order */
        foreach ($orders as $order) {
            $this->container->get(OrderTransporter::class)->transportModel($order, $siteId, $apiKey);
        }
    }

    private function getConfigValue($configName)
    {
        $shop = false;
        if ($this->container->initialized('shop')) {
            $shop = $this->container->get('shop');
        }

        if (!$shop) {
            $shop = $this->container->get('models')->getRepository(\Shopware\Models\Shop\Shop::class)->getActiveDefault();
        }
        $config = $this->container->get('shopware.plugin.cached_config_reader')->getByPluginName('Ootliers', $shop);
        if (!isset($config[$configName])) {
            return null;
        }
        return $config[$configName];
    }
}
