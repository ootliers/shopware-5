<?php

namespace Ootliers\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class ClientFactory
{
    public static function getClient() : ClientInterface
    {
        return new Client();
    }
}
