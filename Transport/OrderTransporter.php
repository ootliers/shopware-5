<?php

namespace Ootliers\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\Order;

class OrderTransporter
{
    const URL = "https://api.ootliers.com/api/v1/site/%s/order";

    /**
     * @var Client
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function transport($order, $siteId, $apiKey)
    {

        $items = [];
        $totalPrice = 0;
        foreach ($order['content'] as $key => $basketRow) {
            $items[] = [
                'name' => $basketRow['articlename'],
                'price' => (string)$basketRow['priceNumeric'],
                'quantity' => $basketRow['quantity'],
            ];
            $totalPrice += $basketRow['priceNumeric'];
        }

        $url = sprintf(static::URL, $siteId);
        $output = [
            'order' => [
                'number' => $order['number'] ?? 'n/a',
                'ordered_at' => (new \DateTime())->format(\DATE_ATOM),
                'total' => (string)$totalPrice,
                'items' => $items,
            ],
        ];

        $this->client->post($url, [
            'json' => $output,
            'headers' => ['Authorization' => $apiKey]
        ]);
    }


    public function transportModel(Order $order, $siteId, $apiKey)
    {
        if (!$order->getNumber()) {
            return;
        }

        $items = [];
        $totalPrice = 0;
        /** @var Detail $basketRow */
        foreach ($order->getDetails() as $basketRow) {
            $items[] = [
                'name' => $basketRow->getArticleName(),
                'price' => (string)$basketRow->getPrice(),
                'quantity' => $basketRow->getQuantity()
            ];
        }
        $url = sprintf(static::URL, $siteId);
        $output = [
            'order' => [
                'number' => $order->getNumber() ?? 'n/a',
                'ordered_at' => $order->getOrderTime()->format(\DATE_ATOM),
                'total' => (string)$order->getInvoiceAmount(),
                'items' => $items,
            ],
        ];

        $this->client->post($url, [
            'json' => $output,
            'headers' => ['Authorization' => $apiKey]
        ]);
    }
}
